//
//  Movies.swift
//  MovieViewer
//
//  Created by Grace and Martin on 27/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import Foundation

enum SearchType {
    case mostPopular
    case highestRated
    case inTheatres
}

struct RequestError : Error {
    var message: String?
}

class MovieLoader {
    
    lazy var webClient : WebClient = {
        let webClient = WebClient(baseUrl: "https://api.themoviedb.org/3/")
        return webClient
    }()
    
    var api_key = "bf2a17354fcd1a212255416a8899d86a"
    
    func requestMovieDetail(movie: Movie ,completion: @escaping(_ : RequestError?, _ : Movie) -> ()) {
        
        guard let _ = webClient.load(path: "movie/\(movie.id)", method: .get, params: ["api_key" : api_key, "language" : "en-US"], body: nil, completion: {
            (response , networkError) in
            if let error = networkError {
                completion(RequestError(message: error.errorDescription), movie)
                return
            }
            
            guard let response = response as? [String : Any]
                
            
            else {
                completion(RequestError(message: "Error creating token from response"), movie)
                return
            }
            
            // use the movieDetail init with decoder to create struct from JSON data and add the movie detail to the movie
            // before returning

            completion(nil, movie)
            
            
        }) else {
            completion(RequestError(message: "load Error"), movie)
            return
        }
        
    }
    
    func getHeaders(searchType: SearchType) -> [String : Any] {
        var headers = ["api_key" : api_key]
         switch searchType {
           case .mostPopular:
               headers["sort_by"] = "popularity.desc"
           case .highestRated:
               headers["sort_by"] = "vote_average.desc"
           case .inTheatres:
               headers["primary_release_date.gte"] = "2020-02-01"
               headers["primary_release_date.lte"] = "2020-02-29"

           }
        return headers
    }
    
    func requestMovies(searchType: SearchType, completion: @escaping(_ : RequestError?, _ : [Movie]?) -> ())  {
        
        guard let _ = webClient.load(path: "discover/movie", method: .get, params: getHeaders(searchType: searchType), body: nil, completion: {
            (response , networkError) in
            if let error = networkError {
                // throw error here
                completion(RequestError(message: error.errorDescription), nil)
                return
            }
            
            guard let response = response as? [String : Any], let results = response["results"] as? NSArray
            
            else {
                completion(RequestError(message: "Error creating token from response"), nil)
                return
            }
            
            var movies = [Movie]()
            let decoder = JSONDecoder()
            do {
                for result in results {
                    let data = try JSONSerialization.data(withJSONObject: result, options: [])
                    let movie = try decoder.decode(Movie.self, from: data)
                    movies.append(movie)
                }

                
            } catch {
                completion(RequestError(message: "load Error"), nil)
                return
            }

            completion(nil, movies)
            
        }) else {
            completion(RequestError(message: "load Error"), nil)
            return
        }
        
    }

}
