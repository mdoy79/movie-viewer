//
//  WebClient.swift
//  Menu
//
//  Created by Grace and Martin on 16/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import Foundation
import Reachability

enum ConnectionStatus {
    case connected
    case unreachable
}

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

final class WebClient {
    
    var baseUrl : String
    let reachability = try! Reachability()
    var connectionStatus : ConnectionStatus!
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {

      let reachability = note.object as! Reachability

      switch reachability.connection {
      case .wifi:
        connectionStatus = .connected
      case .cellular:
          connectionStatus = .connected
      case .unavailable:
        connectionStatus = .unreachable
      case .none:
        print("none")
        }
    }
    
    func load(path: String, method: RequestMethod, params: [String : Any], body: [String : Any]? ,completion: @escaping (Any?, NetworkError?) -> ()) -> URLSessionDataTask? {
        
        if connectionStatus == .unreachable {
            completion(nil, NetworkError.noConnection)
        }

        var request = URLRequest(baseUrl: baseUrl, path: path, method: method, params: params)
        
        if let body = body {
            let json = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            request.httpBody = json
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
           
            var jsonObject: Any? = nil
            if let data = data {
                jsonObject = try? JSONSerialization.jsonObject(with: data, options: [])
            }
                
            if let httpResponse = response as? HTTPURLResponse, (200..<300) ~= httpResponse.statusCode {
                completion(jsonObject, nil)
            } else if let httpResponse = response as? HTTPURLResponse, (401) ~= httpResponse.statusCode {
                completion(nil, NetworkError.custom("Authentication Required"))
            } else {
                let error = (jsonObject as? [String : Any]).flatMap(NetworkError.init) ?? NetworkError.other
                completion(nil, error)
            }
        }
        
        task.resume()
        
        return task
    }
    
}

enum NetworkError: Error {
    case noConnection
    case custom(String)
    case other
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noConnection:
            return "no connection available"
        case .other:
            return "undefined error"
        case .custom(let message):
            return message
        }
    }
}

extension URLRequest {
    init(baseUrl: String, path: String, method: RequestMethod, params: [String : Any]) {
        let url = URL(baseUrl: baseUrl, path: path, params: params, method: method)
        self.init(url: url)
        httpMethod = method.rawValue
        setValue("application/json", forHTTPHeaderField: "Accept")
        setValue("application/json", forHTTPHeaderField: "Content-Type")
    }
}

extension URL {
    init(baseUrl: String, path: String, params: [String : Any], method: RequestMethod) {
        var components = URLComponents(string: baseUrl)!
        components.path += path
        
        switch method {
        case .get, .delete:
            components.queryItems = params.map {
                URLQueryItem(name: $0.key, value: String(describing: $0.value))
            }
        default:
            break
        }
        
        self = components.url!
    }
}

extension NetworkError {
    init(json: [String : Any]) {
        if let message =  json["message"] as? String {
            self = .custom(message)
              } else {
            self = .other
        }
    }
}
