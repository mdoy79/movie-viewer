//
//  Model.swift
//  MovieViewer
//
//  Created by Grace and Martin on 27/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import Foundation

struct MovieDetail {
    var runtime : String
    var tagline : String
    var popularity : Float
    var status : String
    
    // etc
    enum CodingKeys : String, CodingKey {
        case runtime = "runtime"
        case tagline = "tagline"
        case popularity = "popularity"
        case status = "status"
    }
}

struct Movie  {
    var title : String
    var id : Int
    var language : String
    var overview : String
    var backdropPath : String?
    var posterPath : String?
    var voteAverage : Float
    var releaseDate : String
    var movieDetail : MovieDetail?
    
    enum CodingKeys : String, CodingKey {
        case title = "title"
        case id = "id"
        case language = "original_language"
        case backdropPath = "backdrop_path"
        case posterPath = "poster_path"
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
        case overview = "overview"
    }
}


extension Movie : Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try values.decode(String.self, forKey: .title)
        id = try values.decode(Int.self, forKey: .id)
        language = try values.decode(String.self, forKey: .language)
        overview = try values.decode(String.self, forKey: .overview)
        posterPath = try values.decode(String?.self,forKey: .posterPath)
        voteAverage = try values.decode(Float.self, forKey: .voteAverage)
        releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)  ?? ""
        
    }
}

extension MovieDetail : Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        runtime = try values.decode(String.self, forKey: .runtime)
        tagline = try values.decode(String.self, forKey: .tagline)
        status = try values.decode(String.self, forKey: .popularity)
        popularity = try values.decode(Float.self, forKey: .status)

    }
}


