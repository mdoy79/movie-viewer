//
//  detailViewAnimator.swift
//  MovieViewer
//
//  Created by Grace and Martin on 28/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import Foundation
import UIKit

class DetailViewAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
          let toVC = transitionContext.viewController(forKey: .to),
          let snapshot = toVC.view.snapshotView(afterScreenUpdates: true)
          else {
            return
        }

        // 2
        let containerView = transitionContext.containerView
        let finalFrame = transitionContext.finalFrame(for: toVC)


    }
    
    
    override init() {
        
    }
    
}
