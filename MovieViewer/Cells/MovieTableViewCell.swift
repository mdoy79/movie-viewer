//
//  MovieTableViewCell.swift
//  MovieViewer
//
//  Created by Grace and Martin on 27/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(movie: Movie) {
        titleLabel.text = movie.title
        guard let posterPath = movie.posterPath else {
            return
        }
        DispatchQueue.global().async {
            do {
                let url = URL(string: "https://image.tmdb.org/t/p/w92\(posterPath)")!
                let data = try Data(contentsOf: url)
                
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.movieImageView.image = image
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
