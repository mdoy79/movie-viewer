//
//  DetailViewController.swift
//  MovieViewer
//
//  Created by Grace and Martin on 27/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleView: UITextView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var descriptionLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    
    
    var animationScaleValue : CGFloat = 1.3
    let descriptionLabelTopConstraintValue : CGFloat = 380
    let titleTopConstraintValue : CGFloat = 55
    var movie : Movie!
    
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    var startLocation : CGPoint!
    
    lazy var viewHeight : CGFloat = {
        let frame = UIScreen.main.bounds
        let height = max(frame.width, frame.height)
        return height
    }()
    
    func panScale(panValue: CGFloat) -> CGFloat {
        return  (viewHeight + ( panValue * -1)) / viewHeight
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestMovieDetails()
        bindMovieDetails()
        addPanGesture()
        loadPosterImage()
        
    }
    
    fileprivate func bindMovieDetails() {
        descriptionLabel.text = movie.overview
        titleView.text = movie.title
        voteAverageLabel.text = "\(movie.voteAverage)"
        releaseDateLabel.text = movie.releaseDate
    }
    
    fileprivate func addPanGesture() {
        let panGR = UIPanGestureRecognizer(target: self, action: #selector(panDetected))
        view.addGestureRecognizer(panGR)
    }
    
    fileprivate func loadPosterImage() {
        guard let posterPath = self.movie.posterPath else {
            return
        }
               
       DispatchQueue.global().async {
           do {
               let url = URL(string: "https://image.tmdb.org/t/p/w500\(posterPath)")!
               let data = try Data(contentsOf: url)
               
               let image = UIImage(data: data)
               DispatchQueue.main.async {
                   self.movieImageView.image = image
               }
           } catch {
               print(error.localizedDescription)
           }

       }
    }
    
    @objc func panDetected(sender: UIPanGestureRecognizer) {
        
        if sender.state == .began {
            startLocation = sender.location(in: view)
        } else if sender.state == .changed {
            
            let newLocation = sender.location(in: view)
            let dy = startLocation.y - newLocation.y
            if dy < 0 && dy > -300 {
                let scale = panScale(panValue: dy)
                self.movieImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
                self.descriptionLabelTopConstraint.constant = self.descriptionLabelTopConstraintValue - (dy / 4)
                self.titleTopConstraint.constant = self.titleTopConstraintValue + (dy / 4)
                self.view.layoutIfNeeded()
            }
            
        } else if sender.state == .ended {
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
                self.movieImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.descriptionLabelTopConstraint.constant = self.descriptionLabelTopConstraintValue
                self.titleTopConstraint.constant = self.titleTopConstraintValue
                self.view.layoutIfNeeded()
            }, completion: nil)
                
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    fileprivate func requestMovieDetails() {
        let movieLoader = MovieLoader()
        movieLoader.requestMovieDetail(movie: movie, completion: {
            (error, movie) in
            // with more time add in the extra details into a scrollable tableView 
            
        })
    }
    
 
    @IBAction func donePressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
