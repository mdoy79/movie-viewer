//
//  MovieListViewController.swift
//  MovieViewer
//
//  Created by Grace and Martin on 27/02/2020.
//  Copyright © 2020 Grace and Martin. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var movies = [Movie]()
    var tableCellHeight : CGFloat = 73
    @IBOutlet weak var sortTypeLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "movieCell")
        
        // remove empty cells
        tableView.tableFooterView = UIView()

        requestMovies(searchType: .mostPopular)
        sortTypeLabel.text = "Most Popular"
        // Do any additional setup after loading the view.
    }
    
    fileprivate func requestMovies(searchType: SearchType) {
        let movieLoader = MovieLoader()
        
        movieLoader.requestMovies(searchType: searchType, completion: {
            (error, movies) in
            
            guard let movies = movies else {
                return
            }
            self.movies = movies
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let movie = movies[indexPath.row]
        
        cell.setupCell(movie: movie)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let movie = movies[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailController = storyboard.instantiateViewController(identifier: "detailController") as! DetailViewController
        detailController.movie = movie
        
        present(detailController, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableCellHeight
    }
    
    fileprivate func displayMovieOptions() {
        let actionSheet = UIAlertController(title: "Search", message: "", preferredStyle: .alert)
        actionSheet.addAction(UIAlertAction(title: "Highest Rated", style: .default, handler: { _ in
            self.requestMovies(searchType: .highestRated)
            self.sortTypeLabel.text = "Highest rated"
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Most Popular", style: .default, handler: { _ in
            self.requestMovies(searchType: .mostPopular)
            self.sortTypeLabel.text = "Most Popular"
        }))
        actionSheet.addAction(UIAlertAction(title: "In Theatres", style: .default, handler: { _ in
            self.requestMovies(searchType: .inTheatres)
            self.sortTypeLabel.text = "In Theatres"
        }))
        
        present(actionSheet, animated: true, completion: nil)
    }

    @IBAction func optionsPressed(_ sender: Any) {
        
        displayMovieOptions()
        
    }

}

extension MovieListViewController : UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = DetailViewAnimator()
        return animator
    }
}
